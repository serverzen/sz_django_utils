=======
Changes
=======

0.5 - unreleased
================

  * added ``settingsloader`` module overriding django settings

  * first release
