***************
sz_django_utils
***************

A utility library for working with Django.

==========
 Contents
==========

Requirements
============

  * *Django*

sz_django_utils.settingsloader
==============================

Requirements
------------

  * *PyYAML* - for loading settings in YAML format

Info
----

The ``settingsloader`` module is a convenience library for loading settings
from certain non-standard locations in non-standard formats.

Loading settings is done in the following order (replace *myappname* with
the name of your app):

  1. ``$HOME/.django/myappname.settings.ini``
  2. ``$HOME/.django/myappname.settings.yaml``
  3. ``$PWD/.settings.ini``
  4. ``$PWD/.settings.yaml``

The important *key groups* are as follows:

  * *django-overrides* - used to override any settings (this is default behaviour)
  * *django-augments* - used to combine multiple settings already specified elsewhere (ie INSTALLED_APPS)

Please note that all setting names are automatically converted to uppercase.


Usage
-----

Invoking settings loading::

  try:
      from sz_django_utils.settingsloader import load_settings
      load_settings('myappname', globals())
  except ImportError, ex:
      print 'WARNING: Install sz_django_utils for settings.py overrides'

Example ``settings.yaml`` file::

  django-overrides:
    debug: true
    template_debug: true
  django-augments:
    installed_apps:
      - debug_toolbar
    middleware_classes:
      - debug_toolbar.middleware.DebugToolbarMiddleware
