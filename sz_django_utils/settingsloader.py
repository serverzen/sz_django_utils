import logging
import os

_logger = logging.getLogger('sz_django_utils')


class ConditionalLogger(object):
    def __init__(self, logger, conditional):
        self.logger = logger
        self._conditional = conditional

    def info(self, s):
        if self._conditional():
            self.logger.info(s)

    def warn(self, s):
        if self._conditional():
            self.logger.warn(s)


class SettingsManager(object):
    def __init__(self, logger, project_name, namespace):
        self.logger = logger
        self.project_name = project_name
        self.namespace = namespace

    def _set_setting(self, k, v):
        k = k.upper()
        self.namespace[k] = v
        self.logger.info('  %s = %s' % (k, v))

    def load_settings_from_ini(self, p):
        import ConfigParser
        config = ConfigParser.RawConfigParser()
        config.read(p)
        if config.has_section('django-overrides'):
            for k, v in config.items('django-overrides'):
                self._set_setting(k, v)

    def load_settings_from_dict(self, d):
        if 'django-overrides' in d:
            for k, v in d['django-overrides'].items():
                self._set_setting(k, v)
        if 'django-augments' in d:
            for k, v in d['django-augments'].items():
                k = k.upper()
                if k not in self.namespace:
                    self._set_setting(k, v)
                    continue

                oldv = self.namespace[k]
                if isinstance(oldv, (tuple, list)):
                    newv = list(oldv)
                    for x in v:
                        newv.append(x)
                self._set_setting(k, newv)

    def load_settings_from_yaml(self, p):
        try:
            import yaml
        except ImportError:
            self.logger.warn('  please install PyYAML')
            return

        with open(p) as f:
            d = yaml.load(f)
        self.load_settings_from_dict(d)

    def load_settings_from_filepseudo(self, p):
        self.logger.info('Checking for overrides in: %s.ini' % p)
        if os.path.exists(p + '.ini'):
            self.load_settings_from_ini(p + '.ini')
        else:
            self.logger.info("  doesn't exist")

        self.logger.info('Checking for overrides in: %s.yaml' % p)
        if os.path.exists(p + '.yaml'):
            self.load_settings_from_yaml(p + '.yaml')
        else:
            self.logger.info("  doesn't exist")

    def load_settings(self):
        if 'HOME' in os.environ:
            home = os.environ['HOME']
            p = os.path.join(home, '.django',
                             '%s.settings' % self.project_name)
            self.load_settings_from_filepseudo(p)

        self.load_settings_from_filepseudo(os.path.join('.', 'settings'))


LOADED_KEY = 'SZ_DJANGO_UTILS.LOADED'


def _has_loaded():
    return bool(os.environ.get(LOADED_KEY, False))


def load_settings(project_name, namespace):
    _logger.setLevel(logging.INFO)
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    _logger.addHandler(ch)

    logger = ConditionalLogger(_logger, _has_loaded)
    sm = SettingsManager(logger, project_name, namespace)
    sm.load_settings()

    os.environ[LOADED_KEY] = 'true'
