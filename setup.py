from setuptools import setup, find_packages

with open('README.rst') as f:
    readme = f.read().strip()

with open('CHANGES.rst') as f:
    changes = f.read().strip()


setup(name='sz_django_utils',
      version='0.5',
      description='General utilities to make Django easier to work with',
      long_description=readme + '\n\n' + changes,
      classifiers=["Programming Language :: Python"],
      author='Rocky Burt',
      author_email='rocky@serverzen.com',
      url='http://pypi.python.org/pypi/sz_django_utils',
      keywords='django serverzen',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=True,
      install_requires=['django'],
      entry_points='',
      )
